#include <stdbool.h>

#include "bmp_file.h"

static enum read_status check_bmp_header(const struct bmp_header bmpHeader){
    if(bmpHeader.bfType != BMP_FILE_TYPE) return READ_INVALID_SIGNATURE;
    if(bmpHeader.biBitCount != BMP_SUPPORTED_BITS) return READ_INVALID_BITS;

    return READ_OK;
}

static uint32_t calculate_bmp_padding_bytes(uint32_t image_width){
    return (4 - image_width * sizeof (struct pixel) % 4) % 4;
}

static bool read_image(FILE* in, const struct image* img){
    const uint32_t padding_bytes = calculate_bmp_padding_bytes(img -> width);

    for(uint32_t i = 0; i < img -> height; i++){ // read by rows
        // read row of pixels
        if(fread(img -> data + i * img -> width, sizeof (struct pixel), img -> width, in) != img -> width) return false;
        // skip padding
        if(fseek(in, padding_bytes, SEEK_CUR)) return false;
    }

    return true;
}

enum read_status from_bmp(FILE* in, struct image* img){

    // check if in file correct
    if(in == NULL) {
        fprintf(stderr, "Cannot read file\n");
        return READ_FILE_READING_ERROR;
    }

    // read bmp header
    struct bmp_header bmpHeader = {0};
    if(fread(&bmpHeader, sizeof (struct bmp_header), 1, in) != 1) {
        fprintf(stderr, "Cannot read bmp header\n");
        return READ_HEADER_READING_ERROR;
    }

    // check if BMP header correct
    const enum read_status bmp_header_check_status = check_bmp_header(bmpHeader);
    if(bmp_header_check_status) {
        fprintf(stderr, "Incorrect BMP header\n");
        return bmp_header_check_status;
    }

    // create image
    enum image_create_status imageCreateStatus = 0;
    *img = create_image(bmpHeader.biWidth, bmpHeader.biHeight, &imageCreateStatus);
    if(imageCreateStatus){
        fprintf(stderr, "Image creating failed with code %d - %s\n", imageCreateStatus, image_create_status_message[imageCreateStatus]);
        return READ_IMAGE_CREATING_ERROR;
    }

    // read pixels
    const bool read_pixels_success = read_image(in, img);
    if(!read_pixels_success) {
        fprintf(stderr, "Image pixels read failed\n");
        return READ_PIXELS_READ_ERROR;
    }

    return READ_OK;
}

static struct bmp_header create_bmp_header(const struct image* const img){
    // calculate values
    const uint32_t header_size = sizeof (struct bmp_header);
    const uint32_t image_size = sizeof (struct pixel) * (img -> width + calculate_bmp_padding_bytes(img -> width)) * img -> height;
    const uint32_t file_size = header_size + image_size;

    return (struct bmp_header) {
            .bfType = BMP_FILE_TYPE,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = header_size,
            .biSize = BMP_DIB_HEADER_SIZE,
            .biWidth = img -> width,
            .biHeight = img -> height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_SUPPORTED_BITS,
            .biCompression = BMP_COMPRESSION,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = BMP_COLOR_PALETTE,
            .biClrImportant = BMP_IMPORTANT_COLORS,
    };
}

static bool write_bmp_header_to_file(FILE* out, const struct bmp_header bmpHeader){
    if(fwrite(&bmpHeader, sizeof (struct bmp_header), 1, out) != 1) return false;
    return true;
}

static bool write_pixels_to_bmp_file(FILE* out, const struct image* const img){
    const uint32_t padding_bytes = calculate_bmp_padding_bytes(img -> width);
    const uint32_t damn_bytes = 0; // bytes for padding, max 3 bytes
    for(uint32_t i = 0; i < img->height; i++){ // write image by rows
        if(fwrite(img -> data + i * img -> width, sizeof (struct pixel), img -> width, out) != img -> width) return false; // write pixels row
        if(fwrite(&damn_bytes, 1, padding_bytes, out) != padding_bytes) return false; // write padding bytes
    }

    return true;
}

enum write_status to_bmp(FILE* out, struct image const* img){

    // check if in file correct
    if(out == NULL) {
        fprintf(stderr, "Cannot read file\n");
        return WRITE_FILE_READING_ERROR;
    }

    // create BMP header from image
    const struct bmp_header bmpHeader = create_bmp_header(img);

    // write BMP header to file
    const bool header_write_success = write_bmp_header_to_file(out, bmpHeader);
    if(!header_write_success){
        fprintf(stderr, "Error while writing BMP header to file\n");
        return WRITE_HEADER_ERROR;
    }

    // write pixels to file
    const bool pixels_write_success = write_pixels_to_bmp_file(out, img);
    if(!pixels_write_success) {
        fprintf(stderr, "Error while writing pixels to file\n");
        return WRITE_PIXELS_ERROR;
    }

    return WRITE_OK;

}
