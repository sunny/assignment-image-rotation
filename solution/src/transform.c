#include <stdio.h>

#include "transform.h"

static struct pixel* get_pixel_address_by_position(const struct image* const img, uint32_t x, uint32_t y){
    return img -> data + y * img -> width + x;
}

struct image rotate_image_90_counterclockwise(const struct image* const source_img, enum image_create_status* imageCreateStatus){
    // create image
    struct image rotated_image = create_image(source_img -> height, source_img -> width, imageCreateStatus); // swap dimensions
    if(*imageCreateStatus) {
        fprintf(stderr, "Image creating failed with code %d - %s\n", *imageCreateStatus, image_create_status_message[*imageCreateStatus]);
        return (struct image) {0};
    }

    for(uint32_t i = 0 ; i < source_img -> width; i++){
        for(uint32_t j = 0; j < source_img -> height; j++){
            *get_pixel_address_by_position(&rotated_image, rotated_image.width - j - 1, i) = *get_pixel_address_by_position(source_img, i, j);
        }
    }

    return rotated_image;
}
