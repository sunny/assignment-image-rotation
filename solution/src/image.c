#include "image.h"

struct image create_image(uint32_t width, uint32_t height, enum image_create_status* imageCreateStatus){
    *imageCreateStatus = IMAGE_CREATE_OK;

    if(width == 0 || height == 0) { // check if dimensions is correct
        *imageCreateStatus = IMAGE_CREATE_WRONG_DIMENSIONS;
        return (struct image) {0};
    }

    struct image image = {
            .width = width,
            .height = height,
            .data = malloc(sizeof (struct pixel) * width * height)
    };

    if(image.data == NULL) *imageCreateStatus = IMAGE_CREATE_MEM_ERROR;

    return image;
}

void free_image_data(const struct image* const img){
    free(img -> data);
}
