#include <stdio.h>

#include "image.h"
#include "bmp_file.h"
#include "transform.h"

int main( int argc, char** argv ) {

    // check arguments
    if(argc != 3){ fprintf(stderr, "Wrong arguments. Usage: ./image-transformer <source-image> <transformed-image>\n"); }

    // read bmp file to struct image
    FILE * in = fopen(argv[1], "rb");
    struct image image = {0};
    enum read_status bmp_file_read_status = from_bmp(in, &image);
    fprintf(stderr, "BMP file reading ended with code %d - %s\n", bmp_file_read_status, read_status_message[bmp_file_read_status]);
    if(bmp_file_read_status) {
        fclose(in);
        free_image_data(&image);
        return bmp_file_read_status;
    }

    // rotate image
    enum image_create_status imageCreateStatus = IMAGE_CREATE_OK;
    struct image rotated_image = rotate_image_90_counterclockwise(&image, &imageCreateStatus);
    fprintf(stderr, "Image rotating ended with code %d - %s\n", imageCreateStatus, image_create_status_message[imageCreateStatus]);
    if(imageCreateStatus){
        fclose(in);
        free_image_data(&image);
        free_image_data(&rotated_image);
        return imageCreateStatus;
    }

    // save image to bmp_file
    FILE* out = fopen(argv[2], "wb");
    enum write_status bmp_file_write_status = to_bmp(out, &rotated_image);
    fprintf(stderr, "BMP file writing ended with code %d - %s\n", bmp_file_write_status, write_status_message[bmp_file_write_status]);
    if(bmp_file_write_status) {
        fclose(in);
        fclose(out);
        free_image_data(&image);
        free_image_data(&rotated_image);
        return bmp_file_write_status;
    }

    fclose(in);
    fclose(out);
    free_image_data(&image);
    free_image_data(&rotated_image);

    return 0;
}
