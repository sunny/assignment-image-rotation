#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"

#ifndef IMAGE_TRANSFORMER_BMP_FILE_H
#define IMAGE_TRANSFORMER_BMP_FILE_H

#define BMP_FILE_TYPE 0x4D42
#define BMP_SUPPORTED_BITS 24
#define BMP_DIB_HEADER_SIZE 40
#define BMP_PLANES 1
#define BMP_COMPRESSION 0
#define BMP_COLOR_PALETTE 0
#define BMP_IMPORTANT_COLORS 0

#pragma pack(push, 1)
struct bmp_header
{
    // BMP HEADER
    uint16_t bfType; // ID field (42h, 4Dh)
    uint32_t bfileSize; // Size of the BMP file (header + data)
    uint32_t bfReserved; // Application specific
    uint32_t bOffBits; // Offset where the pixel array (bitmap data) can be found

    // DIB header
    uint32_t biSize; // Number of bytes in the DIB header (from this point)
    uint32_t biWidth; // Width of the bitmap in pixels
    uint32_t biHeight; // Height of the bitmap in pixels.
    uint16_t biPlanes; // Number of color planes being used
    uint16_t biBitCount; // Number of bits per pixel
    uint32_t biCompression; // BI_RGB, no pixel array compression used
    uint32_t biSizeImage; // Size of the raw bitmap data (including padding)
    uint32_t biXPelsPerMeter; // Print resolution of the image,
    uint32_t biYPelsPerMeter; // Print resolution of the image,
    uint32_t biClrUsed; // Number of colors in the palette
    uint32_t biClrImportant; // 0 means all colors are important
};
#pragma pack(pop)

static const char* read_status_message[] = {
        "Success read",
        "BMP file invalid signature",
        "BMP file invalid bits",
        "File reading error",
        "BMP header reading error",
        "BMP image pixels reading error",
        "Image creating error"
};

static char* write_status_message[] = {
        "Success write",
        "File reading error",
        "BMP header writing error",
        "BMP image pixels writing error",
};

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_FILE_READING_ERROR,
    READ_HEADER_READING_ERROR,
    READ_PIXELS_READ_ERROR,
    READ_IMAGE_CREATING_ERROR,
};

enum read_status from_bmp(FILE* in, struct image* img);


/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_FILE_READING_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_PIXELS_ERROR,
};

enum write_status to_bmp( FILE* out, struct image const* img);


#endif //IMAGE_TRANSFORMER_BMP_FILE_H
