#include <stdint.h>
#include <stdlib.h>

#include "image.h"

#ifndef IMAGE_TRANSFORMER_TRANSFORM_H
#define IMAGE_TRANSFORMER_TRANSFORM_H

struct image rotate_image_90_counterclockwise(const struct image* const source_img, enum image_create_status* imageCreateStatus);

#endif //IMAGE_TRANSFORMER_TRANSFORM_H
