#include <stdint.h>
#include <stdlib.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

static char* image_create_status_message[] = {
    "Success image create",
    "Wrong dimensions",
    "Memory allocation error",
};


enum image_create_status {
    IMAGE_CREATE_OK,
    IMAGE_CREATE_WRONG_DIMENSIONS,
    IMAGE_CREATE_MEM_ERROR,
};

struct image create_image(uint32_t width, uint32_t height, enum image_create_status* imageCreateStatus);

void free_image_data(const struct image* const img);

#endif //IMAGE_TRANSFORMER_IMAGE_H

